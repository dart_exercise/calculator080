import 'dart:math';

class Cal {
  int x = 0;
  int y = 0;
  var condition;
  int sum = 0;

//Getter
  int getX() {
    return x;
  }

  int getY() {
    return y;
  }

//Setter
  void setX(int x) {
    this.x = x;
  }

  void setY(int y) {
    this.y = y;
  }

//Process + - * ~/
  int Plus(x, y) {
    sum = x + y;
    return sum;
  }

  int Minus(x, y) {
    sum = x - y;
    return sum;
  }

  int Multiply(x, y) {
    sum = x * y;
    return sum;
  }

  int Divide(x, y) {
    sum = x ~/ y;
    return sum;
  }

  int Exponent(x, y){
    dynamic a = pow(x, y);
    sum = a;
    return sum;
  }

  @override
  String toString() {
    return "Condition: " + condition + " num1: $x num2: $y";
  }
}
