import 'dart:io';
import 'cal.dart';

int x = 0, y = 0;
bool end = true;
var obj;


void main() {
  
  print(" --- Start Calculator --- ");
  print(
      "Select condition : \n 1. Plus \n 2. Minus \n 3. Multiplied \n 4. Divide \n 5. Exponent \n 6. Exit");
  var condition = stdin.readLineSync()!;
  obj = new Cal();
// Plus +
  if (condition == "1") {
    obj.condition = "Plus";
    print("input number 1 : ");
    x = int.parse(stdin.readLineSync()!);
    obj.x = x;
    print("input number 2 : ");
    y = int.parse(stdin.readLineSync()!);
    obj.y = y;
    obj.Plus(x, y);
  }
  // Minus -
  else if (condition == "2") {
    obj.condition = "Minus";
    print("input number 1 : ");
    x = int.parse(stdin.readLineSync()!);
    obj.x = x;
    print("input number 2 : ");
    y = int.parse(stdin.readLineSync()!);
    obj.y = y;
    obj.Minus(x, y);
  }
  // Multiply *
  else if (condition == "3") {
    obj.condition = "Multiply";
    print("input number 1 : ");
    x = int.parse(stdin.readLineSync()!);
    obj.x = x;
    print("input number 2 : ");
    y = int.parse(stdin.readLineSync()!);
    obj.y = y;
    obj.Multiply(x, y);
  }

// Divide ~/
  else if (condition == "4") {
    obj.condition = "Divide";
    print("input number 1 : ");
    x = int.parse(stdin.readLineSync()!);
    obj.x = x;
    print("input number 2 : ");
    y = int.parse(stdin.readLineSync()!);
    obj.y = y;
    obj.Divide(x, y);
  }

  // Exponent ^
  else if (condition == "5") {
    obj.condition = "Exponent";
    print("input number 1 : ");
    x = int.parse(stdin.readLineSync()!);
    obj.x = x;
    print("input number 2 : ");
    y = int.parse(stdin.readLineSync()!);
    obj.y = y;
    obj.Exponent(x, y);
  }
  // Exit
  else if (condition == "6") {
    print(" --- END --- ");
    end = false;
  }

  int result = obj.sum;
  if (end == true) {
    print("-------------");
    print(obj);
    print("result : $result ");
    print("-------------");
  }
}
